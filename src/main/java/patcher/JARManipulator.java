package patcher;

import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Scanner;

public class JARManipulator {

    private static String workAreaPath = Main.getWorkAreaPath();


    public static void createWorkArea() {

        File workArea = new File(workAreaPath);
        if (Files.exists(workArea.toPath())) {
            deleteDirectory(workArea);
        }

        //now that the directory is clean, let's go ahead and re-create it.
        workArea = new File(workAreaPath);
        workArea.mkdir();
    }

    private static boolean deleteDirectory(File directory) {
        if(directory.exists()){
            File[] files = directory.listFiles();
            if(null!=files){
                for(int i=0; i<files.length; i++) {
                    if(files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    }
                    else {
                        files[i].delete();
                    }
                }
            }
        }
        return(directory.delete());
    }

    public static boolean copyFile(String source, String destination) {
        try {
//            Files.copy(new File(jarPath).toPath(), new File(workAreaPath + "original.jar").toPath());
            Files.copy(new File(source).toPath(), new File(destination).toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static boolean unpackJar(String jarPath) {

        if(!copyFile(jarPath, workAreaPath + "original.jar")) {
            System.out.println("We couldn\'t copy the JAR... did you delete the OpenRSC client JAR file?");
            return false;
        }

        try {
            Process process = Runtime.getRuntime().exec("jar -xf original.jar", null, new File(workAreaPath));
            process.waitFor();

            if(process.exitValue() == 0)
                return true;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return false;
        }

        return false;
    }

    public static boolean packJar(boolean printOutput) {
        try {
            Process process = Runtime.getRuntime().exec("jar -cfvm patched_client.jar META-INF/MANIFEST.MF com/ darwin/ linux/ net/ orsc/ res/ win-x64/ win-x86/",
                    null, new File(workAreaPath));

            Scanner s = new Scanner(process.getInputStream()).useDelimiter("\\A");
            if(printOutput)
                System.out.println(s.next());

            process.waitFor();

            if(process.exitValue() == 0)
                return true;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return false;
        }

        return false;
    }


}
