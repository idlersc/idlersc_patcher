package patcher;

import org.objectweb.asm.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class ClassManipulator {
    private static String workAreaPath = Main.getWorkAreaPath();

    public static boolean patchClass(String absoluteClassPath) {
        //orsc/ORSCApplet.java:	public void draw()

        final String classPath = workAreaPath + absoluteClassPath;

            try {
            ClassWriter writer = new ClassWriter(0);
            ClassVisitor visitor = new ClassVisitor(Opcodes.ASM4, writer) {}; //'visitor' will forward all events to 'writer'
            ClassReader reader = new ClassReader(new FileInputStream(new File(classPath)));

            reader.accept(new MasterAdapter(visitor), 0); //ties the visitor to our reader

            return saveClassFile(writer.toByteArray(), classPath);
        }
        catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static boolean saveClassFile(byte[] data, String classPath) {
            try (FileOutputStream stream = new FileOutputStream(classPath)) {
                stream.write(data);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
    }
}
