package patcher;



import java.io.File;
import java.util.function.ToDoubleBiFunction;


public class Main {

    public static int PATCHED_FUNCTIONS = 0;
    public static int EXPECTED_PATCHED_FUNCTIONS = 13; //UPDATE THIS WHEN A NEW PATCH IS ADDED

    private static String workAreaPath = "tmp_patcher/";

    public static String getWorkAreaPath() {
        return workAreaPath;
    }

    public static void main(String[] args) {

        JARManipulator.createWorkArea();
        System.out.println("Patching work area created...");
        if (JARManipulator.unpackJar("unpatched_client.jar") == false) {
            System.out.println("Error extracting JAR... is Cache/Open_RSC_Client.jar present? Is JDK8 installed and in your PATH?");
            System.exit(1);
        }
        System.out.println("JAR extracted...");
        System.out.println("Patching...");

        ClassManipulator.patchClass("orsc/mudclient.class");
        ClassManipulator.patchClass("orsc/PacketHandler.class");
        ClassManipulator.patchClass("orsc/ORSCApplet.class");

        if (JARManipulator.packJar(false) == false) {
            System.out.println("Error packing JAR!");
            System.exit(1);
        }

        if(JARManipulator.copyFile(workAreaPath + "patched_client.jar", "patched_client.jar") == false) {
            System.out.println("We successfully patched the jar in tmp_patched/patched.jar but could not copy it outside of the temporary directory. Check the temporary directory for the patched jar.");
            System.out.println(1);
        }


        System.out.println("Patched JAR packed!");
        System.out.println();
        System.out.println("Place patched.jar in the IdleRSC folder!");

        System.out.println("Patched functions: " + Integer.toString(PATCHED_FUNCTIONS));
        System.out.println("Expected patched functions: " + Integer.toString(EXPECTED_PATCHED_FUNCTIONS));

        if(PATCHED_FUNCTIONS != EXPECTED_PATCHED_FUNCTIONS) {
            System.out.println("Soft error: Too little or too many functions were patched.");
            System.exit(1);
        }

        System.exit(0);
    }

    public static boolean isDrawEnabled() {
        return true;
    }

    public static void drawPlayerCallback(int index, int x, int y) {
        int m = index + x + y;
    }
}