# IdleRSC Patcher

This project takes the OpenRSC client JAR file and then patches various methods inside of it. This is necessary for more complex functionality which requires callbacks to IdleRSC, such as painting progress reports on the screen every game frame, or detecting hitsplats. 

This patcher is required to be ran by the project maintainer every time that a new OpenRSC release is made which results in a client break.

## How to Compile

Run mvn package. Copy "target/IdleRSC_Patcher-1.0-SNAPSHOT-jar-with-dependencies.jar" wherever you want to put the patcher JAR at.

## How To Run

Grab the OpenRSC JAR file. Name it "unpatched_client.jar" and place it in the same directory as the patcher JAR. Run the patcher with "java -jar IdleRSC_Patcher-1.0-SNAPSHOT-jar-with-dependencies.jar". Assuming patching was successful, the newly patched client should exist as "patched_client.jar" in the same directory.

If any errors after replacing patched JAR in IdleRSC, IdleRSC may need to be recompiled against the patched JAR.


## How to add new JAR patches (development tutorial)

Before adding patches, it is helpful to know how the patcher actually works at a high level: 

1. It unzips the JAR.
2. It reads the class files.
3. The ASM library is then used to read or modify bytecode. [MasterAdapter](https://gitlab.com/idlersc/idlersc_patcher/-/blob/master/src/main/java/patcher/MasterAdapter.java) looks at the function names in the class and then passes it along to the appropriate "patch adapter" function.
4. For example, OpenRSC's **mudclient.showMessage** is passed to [MessageAdapter](https://gitlab.com/idlersc/idlersc_patcher/-/blob/master/src/main/java/patcher/hookers/MessageAdapter.java).
5. The adapter makes the changes to the class file in memory.
6. The raw class bytecode is then saved to the class file on disk.
7. The JAR is then re-zipped using the modified classes.

Therefore, if you want to add a new patch, you need to:
1. Add a new `if` statement in [MasterAdapter](https://gitlab.com/idlersc/idlersc_patcher/-/blob/master/src/main/java/patcher/MasterAdapter.java)
2. Add a new `XyzAdapter` in `patcher.hookers`.

However, you still need to determine what bytecode/ASM code to throw into your `XyzAdapter`. To do this, you need to install IntelliJ's [ASM Bytecode Viewer](https://plugins.jetbrains.com/plugin/10302-asm-bytecode-viewer). The plugin will allow you to write Java code and then view the bytecode or the corresponding ASMified bytecode.

For example, here are two pictures of "Hello World" being decoded by the viewer:
![Bytecode Viewer](https://i.imgur.com/OprY5DM.png)
![ASMified Bytecode Viewer](https://i.imgur.com/iU3LKOC.png)


From here, it is up to you to learn how to take the ASMified portion of this code and then integrate it into the adapter. There are plenty of examples in `patcher.hookers`!